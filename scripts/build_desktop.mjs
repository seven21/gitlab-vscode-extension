import { buildDesktop } from './utils/desktop_jobs.mjs';

async function main() {
  await buildDesktop();
}

main();
