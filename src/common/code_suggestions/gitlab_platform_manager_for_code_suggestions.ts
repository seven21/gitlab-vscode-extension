import vscode from 'vscode';
import {
  GitLabPlatformManager,
  GitLabPlatformForAccount,
  GitLabPlatform,
} from '../platform/gitlab_platform';
import {
  getAiAssistedCodeSuggestionsConfiguration,
  setAiAssistedCodeSuggestionsConfiguration,
} from '../utils/extension_configuration';
import { getCodeSuggestionsSupport } from './api/get_code_suggestions_support';
import { diffEmitter } from './diff_emitter';

const supportsCodeSuggestions = async (platform: GitLabPlatformForAccount) => {
  let codeSuggestionsEnabled;

  try {
    const result = await platform.fetchFromApi(getCodeSuggestionsSupport());

    codeSuggestionsEnabled = result.currentUser.ide.codeSuggestionsEnabled;
  } catch (e) {
    codeSuggestionsEnabled = false;
  }

  return { platform, codeSuggestionsEnabled };
};

const arePlatformsEqual = (p1: GitLabPlatform | undefined, p2: GitLabPlatform | undefined) => {
  if (p1?.account.id !== p2?.account.id) return false;
  if (p1?.project?.restId !== p2?.project?.restId) return false;
  return true;
};

export class GitLabPlatformManagerForCodeSuggestions {
  readonly #platformManager: GitLabPlatformManager;

  #subscriptions: vscode.Disposable[] = [];

  #onPlatformChangeEmitter = diffEmitter(
    new vscode.EventEmitter<GitLabPlatform | undefined>(),
    arePlatformsEqual,
  );

  constructor(platformManager: GitLabPlatformManager) {
    this.#platformManager = platformManager;
    this.#subscriptions.push(
      this.#onPlatformChangeEmitter,
      this.#platformManager.onAccountChange(async () => {
        this.#onPlatformChangeEmitter.fire(await this.getGitLabPlatform());
      }),
    );
  }

  /**
   * Obtains a GitLab Platform to send API requests to the GitLab API
   * for the code suggestions feature. It follows this logic to decide
   * the correct platform to use:
   *
   * - It returns a GitLabPlatformForProject if an active GitLab project is available.
   * - It returns a GitLabPlatformForAccount if a GitLab account is found with
   * code suggestions support.
   * - If two or more GitLab accounts with code suggestions support are available,
   *  you can specify you preferred account using the "preferredAccount" key.
   * - Otherwise, it returns undefined
   */
  async getGitLabPlatform(): Promise<GitLabPlatform | undefined> {
    const projectPlatform = await this.#platformManager.getForActiveProject(false);
    const codeSuggestionsConfig = getAiAssistedCodeSuggestionsConfiguration();

    if (projectPlatform) {
      return projectPlatform;
    }

    const platforms = await this.#platformManager.getForAllAccounts();

    if (platforms.length === 0) {
      return undefined;
    }

    if (platforms.length === 1) {
      return platforms[0];
    }

    let preferredPlatform = platforms.find(
      platform => platform.account?.username === codeSuggestionsConfig.preferredAccount,
    );

    if (preferredPlatform) {
      return preferredPlatform;
    }

    // Using a for await loop in this context because we want to stop
    // evaluating accounts as soon as we find one with code suggestions enabled
    for await (const result of platforms.map(supportsCodeSuggestions)) {
      if (result.codeSuggestionsEnabled) {
        preferredPlatform = result.platform;
        break;
      }
    }

    if (preferredPlatform?.account) {
      await setAiAssistedCodeSuggestionsConfiguration({
        ...codeSuggestionsConfig,
        preferredAccount: preferredPlatform.account.username,
      });
    }

    return preferredPlatform;
  }

  onPlatformChange = this.#onPlatformChangeEmitter.event;

  dispose() {
    this.#subscriptions.forEach(s => s.dispose());
  }
}
