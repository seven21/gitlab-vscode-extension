import * as vscode from 'vscode';
import { StatePolicy, VisibleState } from './state_policy';
import { diffEmitter } from '../diff_emitter';
import { AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES } from '../constants';
import {
  AI_ASSISTED_CODE_SUGGESTIONS_USER_LANGUAGES,
  getAIAssistedCodeSuggestionsLanguages,
} from '../../utils/extension_configuration';

export const UNSUPPORTED_LANGUAGE: VisibleState = 'code-suggestions-document-unsupported-language';

export class SupportedLanguagePolicy implements StatePolicy {
  #subscriptions: vscode.Disposable[] = [];

  #eventEmitter = diffEmitter(new vscode.EventEmitter<boolean>());

  #isLanguageSupported = false;

  #codeSuggestionLanguages: string[] = [...AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES];

  constructor() {
    this.#setSupportedLanguages();
    this.#subscriptions.push(
      vscode.workspace.onDidChangeConfiguration(e => {
        if (e.affectsConfiguration(AI_ASSISTED_CODE_SUGGESTIONS_USER_LANGUAGES)) {
          this.#setSupportedLanguages();
        }
      }),
      vscode.window.onDidChangeActiveTextEditor(te => this.#checkLanguageSupported(te)),
    );
  }

  async init() {
    this.#checkLanguageSupported(vscode.window.activeTextEditor);
  }

  get engaged() {
    return !this.#isLanguageSupported;
  }

  state = UNSUPPORTED_LANGUAGE;

  onEngagedChange = this.#eventEmitter.event;

  dispose() {
    this.#subscriptions.forEach(s => s.dispose());
  }

  #checkLanguageSupported(textEditor?: vscode.TextEditor) {
    this.#isLanguageSupported = textEditor
      ? this.#codeSuggestionLanguages.includes(textEditor.document.languageId)
      : false;
    this.#eventEmitter.fire(this.engaged);
  }

  #setSupportedLanguages() {
    this.#codeSuggestionLanguages = getAIAssistedCodeSuggestionsLanguages();
  }
}
